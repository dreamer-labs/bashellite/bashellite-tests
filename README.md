# Bashellite testing

Containerized release for bashellite with shunit2 and kcov testing and coverage

This is a temporary image that is used to test the tests, so to speak.

Builds an image based off `repoman/dl-kcov-test` and `repoman/dl-shunit2-test`
and installs a functional bashellite to use while we build shunit2 tests
for bashellite.
